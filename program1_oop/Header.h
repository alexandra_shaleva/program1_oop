#ifndef HEADER_H
#define HEADER_H

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

//����� "������������"
class Storehouse {
	string Content; //���������� ������������
	int Estimation; //������������ ������ ������������
public:
	string Get_Content();
	int Get_Estimation();

	static Storehouse* In_Storehouse(ifstream& ifst); //������� ����� ���������� � ������������
	virtual void In_Data(ifstream& ifst) = 0; //����� ����������� ������� ����� ����������
											  //� ������������, ��� ����� ���������� ���
											  //������� ������ ������������
	virtual void Out_Data(string Content, int Estimation, ofstream& ofst) = 0; //����� ����������� ������� ������ ����������
											  //� ������������, ��� ����� ���������� ���
											  //������� ������ ������������
	int Amount(); //������� �������� ����� ������ ���������� � ������������
	bool Compare(Storehouse* Other); //������� ��������� ����� ������ ���������� ������������
	virtual void Out_Only_Aphorism(string Content, int Estimation, ofstream& ofst); //������� ������ ������ ���������
protected:
	Storehouse() {};
};

//����� "�������"
class Aphorism : public Storehouse {
	string Author; //������ ��������
public:
	void In_Data(ifstream& ifst); //������� ����� ���������� �� ��������
	void Out_Data(string Content, int Estimation, ofstream& ofst); //������� ������ ���������� �� ��������
	void Out_Only_Aphorism(string Content, int Estimation, ofstream& ofst); //������� ������ ������ ���������
	Aphorism() {};
};

//����� "���������"
class Proverb : public Storehouse {
	string Country; //������ ���������
public:
	void In_Data(ifstream& ifst); //������� ����� ���������� � ���������
	void Out_Data(string Content, int Estimation, ofstream& ofst); //������� ������ ���������� � ���������
	Proverb() {};
};

//����� "�������"
class Riddle : public Storehouse {
	string Answer; //�����
public:
	void In_Data(ifstream& ifst); //������� ����� ���������� � �������
	void Out_Data(string Content, int Estimation, ofstream& ofst); //������� ������ ���������� � �������
	Riddle() {};
};

//���� ����������
struct Node {
	Node* Next, *Prev; //��������� �� ����. � ����. ���� ����������
	Storehouse* Cont; //��������� �� ������������
};

//���������
class Container {
	Node* Head; //��������� �� ������ ������
	int Len; //����� ��������� ����������
public:
	void In_Container(ifstream& ifst); //������� ����� �������� � ���������
	void Out_Container(ofstream& ofst); //������� ������ �������� �� ����������
	void Clear_Container(); //������� �������� ����������
	void Sort(); //������� ���������� ����������
	void Out_Only_Aphorism(ofstream& ofst); //������� ������ ������ ���������
	Container(); //�����������
	~Container() { Clear_Container(); } //����������
};

#endif //HEADER_H