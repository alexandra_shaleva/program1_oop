#include "Header.h"

Container::Container() {
	Head = new Node();
	Head->Cont = NULL;
	Head->Next = Head->Prev = Head;
}

void Container::In_Container(ifstream& ifst) {
	Node* Inserted, *Curr;
	Curr = Inserted = NULL;

	while (!ifst.eof()) {
		Inserted = new Node();
		if ((Inserted->Cont = Storehouse::In_Storehouse(ifst))) {
			if (!Len) {
				Head->Cont = Inserted->Cont;
			}
			else {
				Curr = Head->Next;
				Head->Next = Inserted;
				Inserted->Next = Curr;
				Inserted->Prev = Head;
				Curr->Prev = Inserted;
				Head = Inserted;
			}

			Len++;
		}
	}

	Head = Head->Next;
}

void Container::Out_Container(ofstream& ofst) {
	ofst << "Container contains " << Len
		<< " elements." << endl << endl;

	Node* Temp = Head;

	for (int i = 0; i < Len; i++) {
		ofst << i << ": ";
		Temp->Cont->Out_Data(Temp->Cont->Get_Content(), Temp->Cont->Get_Estimation(), ofst);
		ofst << "Amount of punctuation marks in the content of storehouse = " <<
			Temp->Cont->Amount() << endl;
		ofst << endl;
		Temp = Temp->Next;
	}
}

void Container::Clear_Container() {
	for (int i = 0; i < Len; i++) {
		free(Head->Cont);
		Head = Head->Next;
	}

	Len = 0;
}

void Container::Sort() {
	if (Len > 1) {
		Node* First = Head;
		Node* Second = Head->Next;

		Node* Temp = new Node;

		for (int i = 0; i < Len - 1; i++) {
			for (int j = 0; j < Len - i - 1; j++) {
				if (First->Cont->Compare(Second->Cont)) {
					Temp->Cont = First->Cont;
					First->Cont = Second->Cont;
					Second->Cont = Temp->Cont;
				}

				Second = Second->Next;
			}

			First = First->Next;
			Second = First->Next;
		}
	}
}

void Container::Out_Only_Aphorism(ofstream& ofst) {
	ofst << "Only Aphorisms." << endl << endl;

	Node* Temp = Head;

	for (int i = 0; i < Len; i++) {
		ofst << i << ": ";
		Temp->Cont->Out_Only_Aphorism(Temp->Cont->Get_Content(), Temp->Cont->Get_Estimation(), ofst);
		ofst << endl;
		Temp = Temp->Next;
	}
}

Storehouse* Storehouse::In_Storehouse(ifstream& ifst) {
	Storehouse* St = NULL;
	string Temp = "";

	getline(ifst, Temp);

	int K = atoi(Temp.c_str());

	if (K == 1) {
		St = new Aphorism;
	}
	else if (K == 2) {
		St = new Proverb;
	}
	else if (K == 3) {
		St = new Riddle;
	}
	else {
		return 0;
	}

	getline(ifst, St->Content);

	St->In_Data(ifst);

	getline(ifst, Temp);

	St->Estimation = atoi(Temp.c_str());

	return St;
}

string Storehouse::Get_Content() {
	return Content;
}

int Storehouse::Get_Estimation() {
	return Estimation;
}

int Storehouse::Amount() {
	string Alph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789";
	int Amount = 0;

	for (int i = 0; i < Content.size(); i++) {
		if (Alph.find(Content[i]) == -1) {
			Amount++;
		}
	}

	return Amount;
}

bool Storehouse::Compare(Storehouse* Other) {
	return Amount() > Other->Amount();
}

void Storehouse::Out_Only_Aphorism(string Content, int Estimation, ofstream& ofst) {
	ofst << endl;
}

void Aphorism::In_Data(ifstream& ifst) {
	getline(ifst, Author);
}

void Aphorism::Out_Data(string Content, int Estimation, ofstream& ofst) {
	ofst << "It's an Aphorism: " << Content << endl; //������� ����������
	ofst << "Aphorism's author is: " << Author << endl; //������� ������
	ofst << "Subjective estimation of the adage: " << Estimation << endl;
}

void Aphorism::Out_Only_Aphorism(string Content, int Estimation, ofstream& ofst) {
	Out_Data(Content, Estimation, ofst);
}

void Proverb::In_Data(ifstream& ifst) {
	getline(ifst, Country);
}

void Proverb::Out_Data(string Content, int Estimation, ofstream& ofst) {
	ofst << "It's a Proverb: " << Content << endl; //������� ����������
	ofst << "Proverbs's country is: " << Country << endl; //������� ������
	ofst << "Subjective estimation of the adage: " << Estimation << endl;
}

void Riddle::In_Data(ifstream& ifst) {
	getline(ifst, Answer);
}

void Riddle::Out_Data(string Content, int Estimation, ofstream& ofst) {
	ofst << "It's a Riddle: " << Content << endl; //������� ����������
	ofst << "Riddle's answer is: " << Answer << endl; //������� �����
	ofst << "Subjective estimation of the adage: " << Estimation << endl;
}